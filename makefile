#----------------------------------------------------------------------------
#
# Makefile for tools - quick and dirty - needs WATCOM env set
#
#----------------------------------------------------------------------------

CC = wcl386

INCLUDES = -I=.\h

CFLAGS = -za99

SRC=.\src\

all: vdi_info.exe

vdi_info.exe:
  $(CC) $(CFLAGS) $(INCLUDES) $(SRC)\$*.c

clean : .SYMBOLIC
  -@rm *.obj
  -@rm *.err
  -@rm *.sym
  -@rm *.map 

cleanrel : .SYMBOLIC
  -@rm *.obj
  -@rm *.err
  -@rm *.sym
  -@rm *.map
  -@rm vdi_info.exe
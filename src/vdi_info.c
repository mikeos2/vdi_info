/****************************************************************************
 *
 *    vdi_info - extract info from development VDI image
  *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      Jan-Feb 2022
 *
 *  ========================================================================
 *
 *  Description:  Reads development FAT formated VDI file and outputs
 *                the offset from VDI start of the MBR and BPB and
 *                BPB include for use in oemboot.asm.
 * 
 *                wcl386 -za99 vdi_info.c
 *
 *  1.0 January 2022
 *
 *  ===================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "bpbdef.h"
#include "mbrdef.h"

#define SYSLVERSION "1.0"
#define OWMAJOR     (__WATCOMC__/100) - 11
#define OWMINOR     (__WATCOMC__ % 100) / 10

#define TRUE   1
#define FALSE  0

#define SECTORSIZE  512    // bytes per sector

void usage(void);
void GetpartInfo(MBR *ent_buffer, FILE *fhandle);
void OutputInclude(FILE *fhandle);
void WriteOutRecord( int rec_size, char *rec_out, FILE *ohandle);

char DEBUG    = FALSE;  // debug switch
char DUMPINC  = FALSE;  // dump INC to file
char DUMPMBR  = FALSE;  // dump MBR to file
char DUMPBPB  = FALSE;  // dump MBR to file
char WRITEBPB = FALSE;   // write bpb.bin to active partition

// these are for breaking up VDI version
#define MAJORVER(ver) (ver >> 0x10)
#define MINORVER(ver) (ver & 0xFFFF)


// main( ) Start ****************************************************
//
int main(int argc,char *argv[])
{
    int  ch;                              // getopts
    char *Filename    = NULL;             // input VDI file name
    char *AltFilename = NULL;             // Alt filename for input BPB [-w]

    // used to check VDI header by simple compare
    char VDIHeader[ ] = "<<< Oracle VM VirtualBox Disk Image >>>";

    FILE *fhandle = NULL;  // handle for the VDI file

    // structure for holding misc VDI info
    typedef struct {
        char       VDIHeader[39]; // Oracle header string check
        uint32_t   image_sig;     // Image Signature
        uint32_t   image_ver;     // image version 
        uint32_t   header_size;   // header size
        uint32_t   drive_start;   // actual emulated drive data start
        uint16_t   MBRSig;        // MBR signature
        uint8_t    active_part;   // first active part found
        uint32_t   active_offset; // active part offset in sectors from MBR 
        uint8_t    part_type;     // Partition types related to MS DOS
        uint32_t   bootsecoff;    // boot sector offset from VDI start
    } DRIVEINFO;

    // Get program arguments using getopt()
    while ((ch = getopt(argc, argv, "f:imbwh")) != -1)
    {
        switch (ch)
        {
            case 'd':  DEBUG = TRUE;
                       break;

            case 'f':  AltFilename = optarg;
                       break;

            case 'i':  DUMPINC = TRUE;
                       break;                       

            case 'm':  DUMPMBR = TRUE;
                       break;		

            case 'b':  DUMPBPB = TRUE;
                       break;	

            case 'w':  WRITEBPB = TRUE;
                       break;	

            case 'h':  
            default:   usage( );
                       exit(0);
                       break;
        }
    }

    // the input file should be left
    if ((argc -= optind) != 1)
    {
        printf("\nNo inputfile provided!!\n");
        
        usage( );
        exit(1);
    } 
    Filename = argv[optind];

    // make sure write is just not combined with other flags
    if((DUMPINC | DUMPMBR | DUMPBPB) & WRITEBPB) {
        printf("\n*** Disallowed option combination\n");
        usage( );
        exit(1);
    }
    
    // done with args

    // check for input file exists
	if (access(Filename, F_OK ) == 0) {
		printf("Info: Input file found (%s) ", Filename);
    } else {
        printf("Error: Input file not found.\n");
        exit(1);
	}	

    // File exists from previous is true then open input file for reading
	fhandle = fopen(Filename, "rb");
    if (fhandle == NULL) {
        printf("Error: Input file open fail (%d).\n", errno);
        exit(1);
    } else {
        printf("and opened.\n\n");
    }

    // setup and get all the image info into DRIVEINFO structure
    DRIVEINFO *drive_info = (DRIVEINFO *)malloc(sizeof(DRIVEINFO));

    fread(&drive_info->VDIHeader, sizeof(char), strlen(VDIHeader), fhandle);
    if(strncmp(drive_info->VDIHeader, VDIHeader, strlen(VDIHeader) !=0)) {
        printf("Error: Header mismatch.");
        exit(1);
    } else printf("%s\n",drive_info->VDIHeader);
 
    fseek(fhandle, 0x040, SEEK_SET);
    fread(&drive_info->image_sig, sizeof(long), 1, fhandle);
    fread(&drive_info->image_ver, sizeof(long), 1, fhandle);
    fread(&drive_info->header_size, sizeof(long), 1, fhandle);
    printf("Image Version: %d.%d\n", MAJORVER(drive_info->image_ver), MINORVER(drive_info->image_ver));
    printf("Image Sig: %X\n", drive_info->image_sig);
    printf("Header size: %X\n", drive_info->header_size);
    fseek(fhandle, 0x158, SEEK_SET);
    fread(&drive_info->drive_start, sizeof(long), 1, fhandle);
    printf("Drive Start: %X\n", drive_info->drive_start);

    // check MBR signature
    rewind(fhandle);
    fseek(fhandle, (drive_info->drive_start + 0x200 - 0x02), SEEK_SET);
    fread(&drive_info->MBRSig, sizeof(uint16_t), 1, fhandle);
    printf("MBR Sig: %X ", drive_info->MBRSig);
    if(drive_info->MBRSig != 0xAA55) {
        printf("Invalid MBR signature!\n");
        exit(1);
    } else printf("Valid MBR signature!\n");
    
    // dump MBR to mbr.bin if -m option passed
    if(DUMPMBR) {
        printf("*** Dumping MBR ...\n");
        rewind(fhandle);
        fseek(fhandle, (drive_info->drive_start), SEEK_SET);

        uint8_t *buffer = (uint8_t *)malloc(SECTORSIZE);
        fread(buffer, sizeof(uint8_t), SECTORSIZE, fhandle);

        FILE *mbrhandle = fopen("mbr.bin", "wb");
        fwrite(buffer, 1, SECTORSIZE, mbrhandle);
        fclose(mbrhandle);
        free(buffer);
    }

    /* cycle through each of the four partiton entries to:
     * 1. print out entry info
     * 2. find first partition set active
     * 3. get offset to active partion
     */ 
    rewind(fhandle);
    fseek(fhandle, (drive_info->drive_start + PART_ENTRY_OFFSET_1), SEEK_SET);
    
    MBR *ent_buffer = (MBR *)malloc(sizeof(MBR));
    drive_info->active_part = 0; 

    for(uint8_t partition=1; partition<5; partition++){ 
        printf("\nPart num  %d\n", partition);
        GetpartInfo(ent_buffer, fhandle);
        if(ent_buffer->part_state == 0x80 & drive_info->active_part == 0x0) {
            drive_info->active_part   = partition;
            drive_info->active_offset = ent_buffer->part_offset;
            drive_info->part_type     = ent_buffer->part_type;
        }
    }
    free(ent_buffer);

    // no partition to work with - clean and exit 
    if(!drive_info->active_part) {
        printf("\nNo active partition found!!!!\n");
        free(drive_info);
        fclose(fhandle);
        exit(1);
    } else {
        printf("\nActive partition %d found at offset %X\n", drive_info->active_part, 
                                                             drive_info->active_offset);
    }

    drive_info->bootsecoff = (drive_info->drive_start + (drive_info->active_offset * SECTORSIZE));
    printf("Boot sector VDI offset:  %d\n\n", drive_info->bootsecoff);

    // dump BPB to bpb.bin if -m option passed
    if(DUMPBPB) {
        printf("*** Dumping BPB ...\n");
        rewind(fhandle);
        fseek(fhandle, (drive_info->bootsecoff), SEEK_SET);

        uint8_t *buffer = (uint8_t *)malloc(SECTORSIZE);
        fread(buffer, sizeof(uint8_t), SECTORSIZE, fhandle);

        FILE *bpbhandle = fopen("bpb.bin", "wb");
        fwrite(buffer, 1, SECTORSIZE, bpbhandle);
        fclose(bpbhandle);
        free(buffer);
    }

    if(DUMPINC) {
        printf("*** Dumping BPB inc file ...\n\n");        
        rewind(fhandle);
        fseek(fhandle, (drive_info->bootsecoff + 3), SEEK_SET);
        OutputInclude(fhandle);
    }

    // close out input file here because we are either done or this
    // is a BPB write which does not require fhandle is read only
    // but will reopen in write
    fclose(fhandle);        

    // write valid BPB to VDI if -w option passed
    // check -f option for altnate file location and name
    // verify signature and if good write to VDI
    if(WRITEBPB) {
        printf("*** Writing BPB ...\n");

        char *mbrfilename = "newbpb.bin";
        
        // check for exists new BPB image
        if(AltFilename != NULL) mbrfilename = AltFilename;
    	if (access(mbrfilename, F_OK ) == 0) {
		    printf("Info: BPB input file found (%s)\n", mbrfilename);
        } else {
            printf("Error: BPB input file not found.\n");
            exit(1);
	    }
        
        // allocate buffer and read in new BPB image
        uint8_t *buffer1 = (uint8_t *)malloc(512);

        FILE *bpbhandle = fopen(mbrfilename, "rb");
        fread(buffer1, 1, SECTORSIZE, bpbhandle);
        fclose(bpbhandle);          // all done with file so close now 

        // best I can do is verify the sig which is better than nothing
        if(buffer1[511] != 0xAA & buffer1[510] != 0x55) {
            printf("\n*** Error: %s does not contain valid sigature 0xAA55\n");
            free(buffer1);
            free(drive_info);       // kill drive_info
            exit(1);                // get out            
        } else {
            printf("Found valid sigature: %#x%x\n", buffer1[511], buffer1[510]);
        }

        // write routine
        fhandle = fopen(Filename, "r+b");
        fseek(fhandle, (drive_info->bootsecoff), SEEK_SET);
        if(fwrite(buffer1, 1, SECTORSIZE, fhandle) == SECTORSIZE) printf("Full BPB sector written.\n");
        else printf("Error: Bytes written not correct! Verify will fail.\n");
        fclose(fhandle);

        /* Here is where I catch the "oh shit", reopen the VDI and load
         * the just written BPB back into memory then do a quick byte by byte
         * comparison to verify the BPB write.
         */
        uint8_t *buffer2 = (uint8_t *)malloc(SECTORSIZE);

        fhandle = fopen(Filename, "rb");
        fseek(fhandle, (drive_info->bootsecoff), SEEK_SET);
        fread(buffer2, sizeof(uint8_t), SECTORSIZE, fhandle);
        fclose(fhandle);

        // verify
        for(uint16_t i=0;i<SECTORSIZE;i++) {
            if(buffer1[i] != buffer2[i]) {
                printf("\n **** BAD BPB WRITE - Oh shit! *** \n");
                free(buffer1);
                free(buffer2);
                free(drive_info);       // kill drive_info
                exit(1);                // get out  
            }           
        }

        printf("Write verified good - happy face!\n");

        // clean up & close up
        free(buffer1);
        free(buffer2);
    }

    free(drive_info);       // kill drive_info
    printf("All done!\n");
    exit(0);                // made it! get out
}


// usage( )
//
//  Display usage to console
//
void usage(void)
{
    printf("\nvdi_info: get dev image vdi information.\n");
	printf("Version 1.0, compiled %s with OpenWatcom %d.%d\n\n", __DATE__, OWMAJOR, OWMINOR);
    printf("usage: vdi_info [-f alt filename][-i][-m][-p][-w] vdi_filename\n\n");
    printf("   -f #  alternate input filename for -w option.\n");
	printf("   -i    Dump BPB inc to bootpbp.inc.\n");
	printf("   -m    Dump MBR to mbr.bin.\n");
	printf("   -b    Dump active BPB to bpb.bin.\n");
    printf("   -w    Write newmbr.bin to active BPB.\n");       
	printf("   -h    help message.\n\n");
    printf("Valid option combinations:\n");
    printf("  1. -m, -b, or -i single or all at once to dump MBR, BPB and INC.\n");
    printf("  2. -w only or with optional -f <filename> for alternate BPB image to add.\n\n");
    return;
}  //$* end of usage


/* -------------------------------------------------------------------------- 
 * GetpartInfo( )
 *
 * Reads in MBR partition information
 * 
 * In:  Entry buffer and VDI input file handle
 * Out: prints and loads buffer with MBR partition info 
 */
void GetpartInfo(MBR *ent_buffer, FILE *fhandle) 
{
    fread(&ent_buffer->part_state,      sizeof(uint8_t),  1, fhandle);
    fread(&ent_buffer->part_start_head, sizeof(uint8_t),  1, fhandle);
    fread(&ent_buffer->part_start_end,  sizeof(uint16_t), 1, fhandle);
    fread(&ent_buffer->part_type,       sizeof(uint8_t),  1, fhandle);
    fread(&ent_buffer->part_end_head,   sizeof(uint8_t),  1, fhandle);
    fread(&ent_buffer->part_end_end,    sizeof(uint16_t), 1, fhandle);
    fread(&ent_buffer->part_offset,     sizeof(uint32_t), 1, fhandle);
    fread(&ent_buffer->part_sectors,    sizeof(uint32_t), 1, fhandle);

    printf("State: %X\n", ent_buffer->part_state);
    printf("Head:  %X\n", ent_buffer->part_start_head);
    printf("End:   %X\n", ent_buffer->part_start_end);
    printf("Type:  %X\n", ent_buffer->part_type);
    printf("Head:  %X\n", ent_buffer->part_end_head);
    printf("End:   %X\n", ent_buffer->part_end_end);
    printf("Off:   %X\n", ent_buffer->part_offset);
    printf("Sect:  %X\n", ent_buffer->part_sectors); 

    return; 
}


/* -------------------------------------------------------------------------- 
 * OutputInclude( )
 *
 * Pulls in the BPB info and create inc file for use in oemboot.asm
 * 
 * In:  input VDI file handle
 * Out: prints and outputs to BPB include file
 */
void OutputInclude(FILE *fhandle)
{
    int  i;
    char out[128];
    char parttmp[12] = {0};

    BPB_FAT *bpb_buffer = (BPB_FAT *)malloc(sizeof(BPB_FAT));

    fread(&bpb_buffer->BS_OEMName,     sizeof(int8_t),   8, fhandle);
    fread(&bpb_buffer->BPB_BytsPerSec, sizeof(uint16_t), 1, fhandle);  
    fread(&bpb_buffer->BPB_SecPerClus, sizeof(uint8_t),  1, fhandle);
    fread(&bpb_buffer->BPB_RsvdSecCnt, sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_NumFATs,    sizeof(uint8_t),  1, fhandle);
    fread(&bpb_buffer->BPB_RootEntCnt, sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_TotSec16,   sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_Media,      sizeof(uint8_t),  1, fhandle);
    fread(&bpb_buffer->BPB_FATSz16,    sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_SecPerTrk,  sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_NumHeads,   sizeof(uint16_t), 1, fhandle);
    fread(&bpb_buffer->BPB_HiddSec,    sizeof(uint32_t), 1, fhandle);    
    fread(&bpb_buffer->BPB_TotSec32,   sizeof(uint32_t), 1, fhandle); 
    fread(&bpb_buffer->BS_DrvNum,      sizeof(uint8_t),  1, fhandle);    
    fread(&bpb_buffer->BS_Reserved1,   sizeof(uint8_t),  1, fhandle);    
    fread(&bpb_buffer->BS_BootSig,     sizeof(uint8_t),  1, fhandle);    
    fread(&bpb_buffer->BS_VolID,       sizeof(uint32_t), 1, fhandle);    
    fread(&bpb_buffer->BS_VolLab,      sizeof(int8_t),  11, fhandle);
    fread(&bpb_buffer->BS_FilSysType,  sizeof(int8_t),   8, fhandle);

    FILE *ohandle = fopen("bootpbp.inc", "w");

    i = sprintf(out,"; *** BPB output of vdi_info *** \n\n");
    WriteOutRecord(i, out, ohandle);
    for(int x=0;x<8;x++) {parttmp[x] = bpb_buffer->BS_OEMName[x];parttmp[8] = 0;}
    i = sprintf(out,"BS_OEMName      db  \'%8s\'\n\n", parttmp);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_BytsPerSec 	dw  %#06X\n", bpb_buffer->BPB_BytsPerSec);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_SecPerClus 	db  %#04X\n", bpb_buffer->BPB_SecPerClus);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_RsvdSecCnt 	dw  %#06X\n", bpb_buffer->BPB_RsvdSecCnt);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_NumFATs    	db  %#04X\n", bpb_buffer->BPB_NumFATs);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_RootEntCnt 	dw  %#06X\n", bpb_buffer->BPB_RootEntCnt);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_TotSec16    dw  %#06X\n", bpb_buffer->BPB_TotSec16);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_Media       db  %#04X\n", bpb_buffer->BPB_Media); 
    WriteOutRecord(i, out, ohandle);   
    i = sprintf(out,"BPB_FATSz16 	dw  %#06X\n", bpb_buffer->BPB_FATSz16);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_SecPerTrk   dw  %#06X\n", bpb_buffer->BPB_SecPerTrk);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_NumHeads    dw  %#06X\n", bpb_buffer->BPB_NumHeads); 
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BPB_HiddSec     dd  %#010X\n", bpb_buffer->BPB_HiddSec);
    WriteOutRecord(i, out, ohandle);    
    i = sprintf(out,"BPB_TotSec32 	dd  %#010X\n\n", bpb_buffer->BPB_TotSec32);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BS_DrvNum       db  %#04X\n", bpb_buffer->BS_DrvNum);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BS_Reserved1    db  %#04X\n", bpb_buffer->BS_Reserved1);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BS_BootSig      db  %#04X\n", bpb_buffer->BS_BootSig);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"BS_VolID        dd  %#010X\n", bpb_buffer->BS_VolID); 
    WriteOutRecord(i, out, ohandle);
    for(int x=0;x<11;x++) {parttmp[x] = bpb_buffer->BS_VolLab[x];parttmp[12] = 0;}
    i = sprintf(out,"BS_VolLab       db  \'%11s\'\n", parttmp);
    WriteOutRecord(i, out, ohandle);
    for(int x=0;x<8;x++) {parttmp[x] = bpb_buffer->BS_FilSysType[x];parttmp[8] = 0;}
    i = sprintf(out,"BS_FilSysType   db  \'%8s\'\n\n", parttmp);
    WriteOutRecord(i, out, ohandle);
    i = sprintf(out,"; *** end of BPB output ***\n\n");
    WriteOutRecord(i, out, ohandle);

    free(bpb_buffer);
    fclose(ohandle);

    return;
}


/* -------------------------------------------------------------------------- 
 * WriteOutRecord( )
 *
 * For conversion and debugging - just made it easier for me :)
 * 
 * In:  string to output and length
 * Out: nice text
 */
void WriteOutRecord( int rec_size, char *rec_out, FILE *ohandle)
{
    printf("%s", rec_out);
	fwrite(rec_out, 1, rec_size, ohandle);

    return;
}

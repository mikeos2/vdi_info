/***********************************************************************
 *
 *  mbrdef.inc -- Development MBR
 *
 *  ===================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      January 2022
 *
 *  ===================================================================
 *
 *  Description: Structure and defines used for MSDOS MBR
 *
 *  ===================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 **********************************************************************/

#ifndef _MBRDEF_H_
#define _MBRDEF_H_

#include <stdint.h>

// REF: https://www.easeus.com/resource/fat32-disk-structure.htm
//      https://datacadamia.com/io/drive/mbr

#define PART_INACTIVE 0x00
#define PART_ACTIVE   0x80 

#define PART_ENTRY_OFFSET_1    0x1BE    // 1st Partition Entry offset
#define PART_ENTRY_OFFSET_2    0x1CE    // 2st Partition Entry offset
#define PART_ENTRY_OFFSET_3    0x1DE    // 3st Partition Entry offset
#define PART_ENTRY_OFFSET_4    0x1FE    // 4st Partition Entry offset

// Partition types related to MS DOS
#define PART_UNK           0x00     // Unknown or Nothing
#define PART_FAT12         0x01     // 12-bit FAT
#define PART_FAT16         0x04     // 16-bit FAT (Partition Smallerthan 32MB)
#define PART_DOSEXT        0x05     // Extended MS-DOS Partition
#define PART_FAT16BIG      0x06     // 16-bit FAT (Partition Largerthan 32MB)
#define PART_FAT32         0x0B     // 32-bit FAT (Partition Up to2048GB)
#define PART_FAT32LBA      0x0C     // Same as 0BH, but uses LBA1 13h Extensions
#define PART_FAT16BIGLBA   0x0E     // Same as 06H, but uses LBA1 13h Extensions
#define PART_DOSEXTLBA     0x0F     // Same as 05H, but uses LBA1 13h Extensions

/*
   Offset  Description                                           Size
   ******  ****************************************************  ********
    0x00   Current State of Partition(00h=Inactive, 80h=Active)  uint8_t
    0x01   Beginning of Partition - Head                         uint8_t
    0x02   Beginning of Partition - Cylinder/Sector (See Below)  uint16_t
    0x04   Type of Partition                                     uint8_t
    0x05   End of Partition - Head                               uint8_t
    0x06   End of Partition - Cylinder/Sector                    uint16_t
    0x08   Number of Sectors Betweenthe MBR and the First        uint32_t
           Sector in the Partition
    0x0C   Number of Sectors in thePartition                     uint32_t
*/

// Partition entry - 16 bytes
typedef struct _MBR {
    uint8_t  part_state;
    uint8_t  part_start_head;
    uint16_t part_start_end;  
    uint8_t  part_type;
    uint8_t  part_end_head;
    uint16_t part_end_end;
    uint32_t part_offset;
    uint32_t part_sectors;
} MBR;

#endif

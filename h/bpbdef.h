/***********************************************************************
 *
 *  bpbdef.inc -- Development BPB
 *
 *  ===================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      March 2019
 *
 *  ===================================================================
 *
 *  Description: Structure used for FAT16 drive with extension
 *
 *  ===================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 **********************************************************************/

#ifndef _BPBDEF_H_
#define _BBPDEF_H_

#include <stdint.h>

typedef struct _BPB_FAT {

    int8_t   BS_OEMName[8];          // OEM label

    // matches older bpb.h
    uint16_t BPB_BytsPerSec;         // Number of bytes per sector (512) Must be one of 512, 1024, 2048, 4096.
    uint8_t  BPB_SecPerClus;         // Number of sectors per cluster Must be one of 1, 2, 4, 8, 16, 32, 64, 128.
    uint16_t BPB_RsvdSecCnt;         // reserved sectors, in 12/16 usually 1 for BPB, FAT32 uses 32
    uint8_t  BPB_NumFATs;            // number of FATs, 
    uint16_t BPB_RootEntCnt;         // root directory entries, 0 for FAT32. 512 is recommended for FAT16.
    uint16_t BPB_TotSec16;           // 16-bit total count of sectors on the volume, if 0 see BPB_TotSec32
    uint8_t  BPB_Media;              // is no longer usually used, F8 HD FA Ram Disk
    uint16_t BPB_FATSz16;            // sectors per 1 FAT copy
    uint16_t BPB_SecPerTrk;          // sectors per track
    uint16_t BPB_NumHeads;           // number of heads
    uint32_t BPB_HiddSec;            // hidden sectors
    uint32_t BPB_TotSec32;           // big total sectors  BPB_TotSec32 * BPB_BytsPerSec = HD size
    // end of old bpb.h plus 6 db (?) 

    uint8_t  BS_DrvNum;               // boot unit
    uint8_t  BS_Reserved1;            // Reserved (used by Windows NT). FAT always 0
    uint8_t  BS_BootSig;              // 0x29 indicates next 3 fields in the boot sector present
    uint32_t BS_VolID;                // volume serial number
    int8_t   BS_VolLab[11];           // volume label
    int8_t   BS_FilSysType[8];  	  // filesystem id

} BPB_FAT;

#endif
